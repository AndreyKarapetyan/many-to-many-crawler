const got = require("got");
const cheerio = require("cheerio");
const f = async (arr1, arr2) => { // we search in the first array the values of the second
	let promiseArr = [], answer = []; // an array of promises, and the final answer
	const normalize = (str) => { // get rid of confusing stuff
		if(str.includes("http://"))
			str = str.split("http://")[1];
		if(str.includes("https://"))
			str = str.split("https://")[1];
		if(str.includes("www."))
			str = str.split('www.')[0] + str.split('www.')[1];
		if(str.endsWith('/'))
			str = str.substring(0, str.length - 1);
		return str;
	}
	let arr2Normalized = arr2.map((v, i) => normalize(v)); // however, we should keep the original
	for(let i = 0; i < arr2Normalized.length; i++) {
		for(let j = 0; j < arr1.length; j++)
			promiseArr.push(got(arr1[j], {timeout: 60000}).then((response) => { // we push for the final settlement
				const $ = cheerio.load(response.body);
				var linkarr = Array.from($('a')); // to be able to iterate with for loop
				let relforthis = null, isFound = false; // the final rel attribute for the url
				for(let k = 0; k < linkarr.length; k++) {
					let href = normalize(linkarr[k].attribs.href);
					let rel = linkarr[k].attribs.rel;
					if(href === arr2Normalized[i]) { // the site has the necessary url
						isFound = true;
						if(!rel || rel.includes("nofollow")) // skip these bad values
							continue;
						else
							relforthis = rel; // or have the last one
					}
				}
				if(isFound)
					answer.push({
						URL: arr1[j],
						statusCode: response.statusCode,
						status: "active",
						rel: relforthis,
						checkURL: arr2[i]
					});
				else
					answer.push({
						URL: arr1[j],
						statusCode: response.statusCode,
						status: "missing",
						rel: null,
						checkURL: arr2[i]
					});					
			}).catch((e) => { // 404
				answer.push({
					URL: arr1[j],
					statusCode: 404,
					status: "broken",
					rel: null,
					checkURL: arr2[i]
				});
			}));
	}
	let a = await Promise.allSettled(promiseArr).then(() => answer); // return the final answer
	return a;
};
f(["https://myjob.amsl", "https://staff.am"],
  ["https://t.me/staffam", "something", "https://facebook.com/mystaff.am"])
.then((ans) => console.log(ans));
